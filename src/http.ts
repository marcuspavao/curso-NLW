import express, { response } from "express"
import './database'
import { createServer} from 'http'
import {Server, Socket} from 'socket.io'
import path from 'path'
import {routes} from './routes'

const app = express()

app.use(express.static(path.join(__dirname,"..","public")))
app.set("views", path.join(__dirname, "..", "public"))
app.engine("html", require("ejs").renderFile)
app.set("view engine", "html")

app.get("/", (req, res)=>{
    return res.render("html/client.html")
 })
 app.get("/admin", (req, res)=>{
    return res.render("html/admin.html")
 })
const http = createServer(app)
const io = new Server(http) // Ambos protocolos ficam disponíveis

io.on("connection", (socket: Socket) => {
    console.log("Se connectou", socket.id)
})

app.use(express.json())
app.use(express.urlencoded())
app.use(routes)

export {http, io}