import {getCustomRepository, Repository} from "typeorm"
import { User } from "../entities/User"
import {UsersRepository} from '../repositories/UsersRepository'

interface IUsersServices {
    email : string
}

class UserServices {
    private userRepository : Repository<User>
    
    constructor(){
        this.userRepository = getCustomRepository(UsersRepository)
    }


    async create({ email }: IUsersServices) {       
    

        const userAlreadyexists = await this.userRepository.findOne({ email })
        
        if(userAlreadyexists){
            return userAlreadyexists
        }
        
        const users = this.userRepository.create({
            email
        }
        )
        await this.userRepository.save(users) 

        return users;    

    }

    async findByEmail(email: string) {
        const userExists = await this.userRepository.findOne({
            email
        })
        if (userExists) {
            return userExists 
        }

        return null
    }

}

export {UserServices}