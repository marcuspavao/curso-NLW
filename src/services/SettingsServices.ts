import {getCustomRepository, Repository} from "typeorm"
import { Setting } from "../entities/Setting"
import {SettingsRepository} from '../repositories/SettingsRepository'

interface ISettingsServices {
    chat: boolean
    username: string
}

class SettingServices {
    private settingsRepository : Repository<Setting>
    
    constructor(){
        this.settingsRepository = getCustomRepository(SettingsRepository)
    }

    async create({ chat, username }: ISettingsServices) {      

        const userAlreadyexists = await  this.settingsRepository.findOne({ username })
        
        if(userAlreadyexists){
            throw new Error("Usuario já existe")
        }
        
        const settings =  await this.settingsRepository.create({
            chat, username
        }
        )
        await  this.settingsRepository.save(settings) 

        return settings;    

    }

    async findByUsername(username: string){
        const settings = await this.settingsRepository.findOne({username,})
        return settings
    }

    async update(username: string, chat: boolean){
        await this.settingsRepository.createQueryBuilder().
        update(Setting)
        .set({chat})
        .where("username = :username",{ username})
        .execute()
    }

}

export {SettingServices}