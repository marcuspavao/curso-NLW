import {getCustomRepository, Repository} from "typeorm"
import { Connection } from "../entities/Connection"
import { ConnectionRepository } from '../repositories/ConnectionsRepository'

interface IConnectionsServices {
    admin_id?: string
    user_id: string
    socket_id: string
    id?: string
}

class ConnectionServices {
    private connectionRepository : Repository<Connection>
    
    constructor(){
        this.connectionRepository = getCustomRepository(ConnectionRepository)
    }


    async create({ admin_id, user_id, id, socket_id }: IConnectionsServices) {       
    
        const connections = this.connectionRepository.create({
            admin_id, user_id, id, socket_id
        }
        )
        await this.connectionRepository.save(connections) 

        return connections;    

    }

    async findByUserId(user_id: string) {
        const connection = await this.connectionRepository.findOne({
            user_id
        })

        return connection
    }

    async findWithoutAdmin () {
        const connections = await this.connectionRepository.find({
            where: {admin_id: null}, 
            relations: ["user"]
        })
        return connections
    }

    async findBySocketID(socket_id: string) {
        const connections = await this.connectionRepository.findOne({socket_id})
        return connections
    }

    async updateAdminID(user_id: string, admin_id: string) {
        await this.connectionRepository.createQueryBuilder().
        update(Connection)
        .set({admin_id})
        .where("user_id = :user_id",{ user_id})
        .execute()
    }
    //Adicionei o serviço de deletar o socket_id
    async deleteBySocketId(socket_id: string){
    await this.connectionRepository.createQueryBuilder()
    .delete()
    .from(Connection)
    .where("socket_id = :socket_id",{ socket_id})
    .execute()
    }

    }



export {ConnectionServices}