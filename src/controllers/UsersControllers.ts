import { Request, Response } from 'express'
import { UserServices } from '../services/UsersServices'


class UsersController {
    async create(req: Request, res: Response) {

        const { email } = req.body

        const usersService = new UserServices()

        try {
            const users = await usersService.create({ email })

            return res.json(users)
        }
        catch(err) {
            return res.status(400).json({
                msg: err.message
            })

        }
    }
}

export { UsersController }