import { Request, Response } from "express";
import { MessagesServices } from "../services/MessagesServices";


class MessagesControllers{

   async create(req:Request, res: Response){
    const {admin_id, user_id, text} = req.body
    const messageService = new MessagesServices

    try {
        const messages = await messageService.create({admin_id, user_id, text})

        res.json(messages)
    }
    catch(err) {
        return err
   }    
}
    async showByUser(req:Request, res: Response){
        const { id } = req.params

        const messageService = new MessagesServices()

        try {
            const list = await messageService.listByUser(id)
    
            res.json(list)
        }
        catch(err) {
            return err
       }


    }
}

export {MessagesControllers}